import { Router } from 'express';

// create our job queue
import kue from 'kue';
import mongoose from 'mongoose';
import helloWorld from '~/hello-world/rest';
import crudOperations from '~/crud-operations/rest';
import authorization from '~/authorization/rest';
import Contact from '~/models/contact';

const router = Router();

const queue = kue.createQueue({
  prefix: 'Minh', // Can change this value incase you're using multiple apps using same
               // redis instance.
  redis: {
    host: 'localhost',
    port: 6379 // default
  }
});

router.get('/', (req, res) => {
  res.send('app-root-1');
});

// Chức năng chạy kiểm thử
router.post('/contact-us', async (req, res) => {
  // eslint-disable-next-line no-var
  req.body.title = 'Submit contact form';
  queue.create('contact', req.body)
  .priority('high')
  .save();
  // eslint-disable-next-line func-names
  queue.process('contact', function(job, done){
    // eslint-disable-next-line no-param-reassign
    job.data._id = new mongoose.Types.ObjectId();
    const contact = new Contact(job.data);
    contact.save();
    done();
  });
  res.json({success: 'Successfully assigned job to the worker'});
});

router.use('/hello-world', helloWorld);
router.use('/crud-operations', crudOperations);
router.use('/authorization', authorization);
export default router;
