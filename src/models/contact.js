const mongoose = require('mongoose');

const contactSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId
},{strict:false});

module.exports = mongoose.model('contact', contactSchema);